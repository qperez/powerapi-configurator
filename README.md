# powerapi-configurator :zap: :electric_plug:

## About powerapi-configurator 

This repository contains ressources allowing to use more easily the [PowerAPI]() software. Here, we use two
PowerAPI software:
* [HWPC-Sensor](https://powerapi-ng.github.io/hwpc-sensor.html): HardWare Performance Counter (HWPC) Sensor is a tool 
that monitor the Intel CPU performance counter and the power consumption of CPU. Hwpc-sensor uses the RAPL 
(Running Average Power Limit) technology to monitor CPU power consumption. This technology is only available on 
Intel Sandy Bridge architecture or higher.

* [SmartWatts](https://powerapi-ng.github.io/smartwatts.html#smartwatts): is a software-defined power meter 
based on the PowerAPI toolkit. SmartWatts is a configurable software that can estimate the power consumption of software in real-time.
Also, it is able to estimate the power consumption of docker containers. 

These software are used in their dockerized version:
* [https://hub.docker.com/r/powerapi/smartwatts-formula](https://hub.docker.com/r/powerapi/smartwatts-formula)
* [https://hub.docker.com/r/powerapi/hwpc-sensor](https://hub.docker.com/r/powerapi/hwpc-sensor)

All the data produced by HWPC-sensor and SmarttWatts are stored in containerized databases: MongoDB and InfluxDB.

## Table of Contents

<!-- TOC -->
* [About powerapi-configurator](#about-powerapi-configurator)
* [Table of Contents](#table-of-contents)
* [Prerequisites :construction:](#prerequisites--construction-)
  * [Access to RAPL interface](#access-to-rapl-interface)
  * [Cgroup](#cgroup)
  * [Docker and Docker Compose](#docker-and-docker-compose)
* [How it works?](#how-it-works)
  * [Overview](#overview)
    * [HPWC-Sensor](#hpwc-sensor)
    * [Mongo-DB database](#mongo-db-database)
    * [SmartWatts](#smartwatts)
    * [InfluxDB](#influxdb)
    * [Grafana](#grafana)
  * [Technical view](#technical-view)
    * [Network](#network)
    * [HWPC-Sensor](#hwpc-sensor)
    * [Mongo-DB](#mongo-db)
    * [SmartWatts](#smartwatts-1)
    * [InfluxDB](#influxdb-1)
* [Workflow to install monitoring on your machine](#workflow-to-install-monitoring-on-your-machine)
  * [1) First, match the prerequisites](#1--first-match-the-prerequisites)
  * [2) Create cgroup for process](#2--create-cgroup-for-process)
  * [3) Set environment variables in .env files and in the stack_powerapi.yml](#3--set-environment-variables-in-env-files-and-in-the-stackpowerapiyml)
  * [4) Generation of configuration files](#4--generation-of-configuration-files)
  * [5) Run docker compose](#5--run-docker-compose)
* [Video Tutorial](#video-tutorial)
* [Data visualization with Grafana](#data-visualization-with-grafana)
  * [First Logging](#first-logging)
  * [Create Data Source](#create-data-source)
  * [Create a power consumption visualization chart](#create-a-power-consumption-visualization-chart)
<!-- TOC -->

---
## Prerequisites :construction:

### Access to RAPL interface
PowerAPI tools uses the RAPL values to monitor the CPU power consumption. Since the [PLATYPUS](https://platypusattack.com/) 
attacks, the kernel RAPL interface in
Linux is usable only if the user is logged as root. A "workaround" to manage the access to the RAPL interface is to use 
a 'sysfs.conf' configuration file.

### Cgroup

To correctly estimate the power consumption of processes and/or containers the HWPC-Sensor uses Cgroup.
Cgroup (also named control groups) is a Linux kernel feature that limits, accounts for, isolates 
the resource usage (CPU, memory, disk I/O, network, etc.) of a collection of processes. Cgroup provides interfaces
to monitor performances for each process added to it. 

To monitor and estimate precise consumption for each process or container cgroup must be installed in version 1. 
Indeed, there are two versions of cgroup (version 1 and version 2) but the HWPC-Sensor can only monitor perf_event interface in 
cgroup version 1. 

To know the version of cgroup installed is it possible to check the existence of `cgroup.controllers` file in 
`/sys/fs/cgroup/`. If `cgroup.controllers` is present then cgroup v2 is installed. 

To boot the host with cgroup v1, add the following string: `systemd.unified_cgroup_hierarchy=0` to the `GRUB_CMDLINE_LINUX` 
line in `/etc/default/grub` and then run `sudo update-grub`. 

### Docker and Docker Compose
Docker and Docker Compose must be installed on the local machine where is deployed the power consumption measurements. 

---
## How it works?

### Overview

The docker compose file `stack_powerapi.yml` contains the different containers needed to monitor the power consumption.
The system of power consumption measurements is entirely dockerized for an easier deployment.


As shown in the figure below, the stack is composed of 5 elements, each of them has a role in monitoring process. 
In the figure, the monitoring is doing on a CI/CD software; here Jenkins; it is the same if the monitoring 
is applied to other processes or containers. 

![Read more words](docs/figures/figure_monitoring_jenkins_overview.png)

#### HPWC-Sensor

The HWPC-Sensor collects 8 performances and CPU metrics using through performance interfaces anc CPU infos in `/sys`.
Information is collected at a specific frequency, fixed by default to 500ms in a configuration file (`config_file_hwpc.json`).
It also fixes different parameters: debug mode, metrics to monitor, connectors for data saving, etc. 

* **RAPL**: Running Average Power Limit (RAPL)
* **TSC**: Time Stamp Counter (TSC)
* **APERF**: Average CPU frequency over a time period 
* **MPERF**: Maximum CPU frequency over a time period
* **CPU_CLK_THREAD_UNHALTED:REF_P**: Reference base clock (133 Mhz) cycles when thread is not halted
* **CPU_CLK_THREAD_UNHALTED:THREAD_P**: Cycles when thread is not halted 
* **LLC_MISSES**: Last-Level Cache misses
* **INSTRUCTIONS_RETIRED**: count how many instructions were completely executed.  

After collecting values, HWPC-Sensor send them to a dockerized Mongo-DB database through software port (here localhost:27017)

#### Mongo-DB database

The Mongo-DB database stores the values (raw consumptions) coming from the HWPC-sensor. It is used here as a "queue", 
raw consumptions are pushed by HWPC-Sensor and consumed by SmartWatts. It exposes a 27017 port to interact with HWPC-sensor
and Smartwatts.

#### SmartWatts

SmartWatts is a software-defined power meter based on the PowerAPI toolkit. SmartWatts is a configurable software 
that can estimate the power consumption of software in real-time. SmartWatts used values provided HWPC-Sensor then 
stored in the Mongo-DB database. Like HWPC-Sensor it used a configuration file (`config_file_sw.json`) providing 
information about CPU features and frequencies.

Metrics collected by HWP-Sensor are then used as inputs for a power model that estimate the power consumption of each software. 
The model can derive from the reality, each time the cpu-error-threshold is reached it learns a new power model, 
using the previous reports. Thus, **Smartwatts is able to estimate a "real" power consumption in Watts for each process
or containers monitored**. It pushed values computed into an InfluxDB database reachable at the 8086 port. 

For more information about Smartwatts and the formula to compute an accurate consumption see:
* Publication: [SmartWatts: Self-Calibrating Software-Defined Power Meter for Containers](https://hal.inria.fr/hal-02470128)
* Documentation: [https://powerapi-ng.github.io/smartwatts.html](https://powerapi-ng.github.io/smartwatts.html)

#### InfluxDB

Influx DB container stores the values computed by SmartWatts in a database named "power_consumption". This container
exposes 8086 port. 

#### Grafana

Grafana is used here to visualize data stored in the Influx DB database.

### Technical view

Here we described technically how the power consumption monitoring works with a system view. The figure below presents from a technical point 
of view an example of pipeline monitoring. 

![Read more words](docs/figures/figure_monitoring_jenkins_technical_view.png)

#### Network
Containers in the stack used the localhost interface to communicate with each other. 

#### HWPC-Sensor

The HWPC-sensor gets metrics for monitored processes and containers through RAPL interface in `/sys/devices/virtual/powercap/intel-rapl`
and cgroup `perf_event` in `/sys/fs/cgroup/perf_event`. Each cgroup created in perf_event are monitored by the HWPC-Sensor. 
For instance, in the figure above there are two cgroup: 
* Docker: for Docker, this allows to monitor each container deployed on the machine.
* Jenkins: for a process like Jenkins, each child process create by the monitored process (maven, git, javac, etc.) will 
be taken in the consumption measured. 

HWPC-sensor used the four following volumes to collect the different metrics:
```
- /sys:/sys
- /var/lib/docker/containers:/var/lib/docker/containers:ro
- ./config_file.json:/config_file.json 
```
One of them concerns JSON configuration file sharing (config_file_hwpc.json). As mentioned before this file is used 
to fix different parameters used by the sensor (interfaces to monitor, sampling frequency, debug_mode, etc.).
This file is generated automatically with a script, see section [4) Generation of configuration files](#4--generation-of-configuration-files).

#### Mongo-DB

The Mongo-DB database stores the values (raw consumptions) coming from the HWPC-sensor. It exposes a 27017 port to 
interact with HWPC-sensor and Smartwatts.

#### SmartWatts

SmartWatts consumes the data stored in MongoDB using the 21017 port. It estimates a consumption using 
raw measurements produced by the HWPC-Sensor. After that, it pushes data into an InfluxDB database reachable
at the port 8086.

#### InfluxDB

Influx DB database contains values computed by SmartWatts. These values are stored in a database named `power_consumption`.

This database have a field `power` where energy consumption for each process/container (`target`) are stored. Using the following
SQL request in a terminal you can obtain the data for each `target`: 

```SQL
SELECT power FROM "power_consumption" GROUP BY target
```

---
## Workflow to install monitoring on your machine

### 1) First, match the prerequisites

### 2) Create cgroup for process

The second step consists of creation of the cgroup for processes monitoring. To do so, use the following
commands in a terminal:
```
$ cgcreate -g perf_event:myprocess 
$ cgclassify -g perf_event:myprocess $(pidof myprocess)
```

For Jenkins the command is similar but the PID of Jenkins is obtained from a different way. Indeed, Jenkins is Java WAR launched on 
the host machine. Thus, for Jenkins please use the following commands:
```
$ cgcreate -g perf_event:jenkins 
$ cgclassify -g perf_event:jenkins $(ps -Af | grep "jenkins" | grep -v grep | awk '{print$2}') 
```

### 3) Set environment variables in .env files and in the stack_powerapi.yml

If you want to define specific port or login/password for the different containers, you can use `*.env` files and 
docker compose file. :warning: **Environment variables must be set here because after they are used by the `config_files_powerapi_generator.sh` Shell script to generate configuration
files for HWPC-Sensor and SmartWatts** :warning:

### 4) Generation of configuration files
In this step the two configuration files used by HWPC-sensor and Smartwatts will be generated with 
the `config_files_powerapi_generator.sh` Shell script. **:warning: Check that fields concerning CPU frequencies' 
are defined in `config_file_sw.json`, if it is not the case please set manually.**  

Excerpt of generated config_file_sw.json
```json
{

  ...
  "cpu-frequency-base": 1900,
  "cpu-frequency-min": 400,
  "cpu-frequency-max": 4200,
  ...
}
```

### 5) Run docker compose

To begin the power monitoring please use the following command in a terminal:
```
$ DOCKER_POWERAPI_STORAGE=my_path_for_persistent_storage docker compose -f stack_powerapi.yml up
```
`DOCKER_POWERAPI_STORAGE` is the variable defining the path for the persistent storage of MongoDB, InfluxDB and Grafana, example:
```
$ DOCKER_POWERAPI_STORAGE=/home/user1/my_directory/ docker compose -f stack_powerapi.yml up
### Don't forget the '/' at the end of the path
```
---
## Video Tutorial

[https://youtu.be/rUuglL1RPoI](https://youtu.be/rUuglL1RPoI)

---
## Data visualization with Grafana

To visualize data Grafana you can access to the interface using the following URL: [http://localhost:3000](http://localhost:3000)

### First Logging
The first logging to Grafana is done with "_admin_" for the username and "_admin_" for the password. You can change this after this
window. 

![](docs/figures/grafana/grafana_login.png)

### Create Data Source

To create charts with data in InfluxDB database a data source need to be created in Grafana. 
In the left panel, please click on "_Configuration_" and then on "_Data Source_"
![](docs/figures/grafana/grafana_data_source_1.png)
---

Please choice "_InfluxDB_" in the list of available data sources.
![](docs/figures/grafana/grafana_data_source_2.png)
---

Set the name and the URL as shown by the figure below. By default, the URL is: [http://localhost:3000](http://localhost:3000).
![](docs/figures/grafana/grafana_data_source_3.png)

---
Set the database name with: "_power_consumption_" and click on "_Save & Test_".
![](docs/figures/grafana/grafana_data_source_4.png)

---
Then a message mentioning "_data source working, 1 measurements found_" without error could appear 
![](docs/figures/grafana/grafana_data_source_5.png)

### Create a power consumption visualization chart

In the "_Dashboards_" interface, click on the button "_New_" and then on "_New Dashboard_".
![](docs/figures/grafana/grafana_dashboard_1.png)

---
Click on "Add a new panel"
![](docs/figures/grafana/grafana_dashboard_2.png)

---
1. Firstly, please set the name of the "_Panel_" >> "_Title_"
2. Select the correct data source, i.e. data source corresponding to the InfluxDB dedicated to the power consumption storage.
3. Click on the :pencil2: "pencil" icon 
![](docs/figures/grafana/grafana_dashboard_3.png)

---
In the SQL window please type the following SQL request:
```SQL
SELECT power FROM "power_consumption" GROUP BY target
```

In the "_alias by_" text field, please type `$tag_target`.
![](docs/figures/grafana/grafana_dashboard_4.png)

---
In the left panel, you can specify the measure unit in Watt (W).
![](docs/figures/grafana/grafana_dashboard_5.png)

---
Lastly click on "_Apply_" to save your panel.
![](docs/figures/grafana/grafana_dashboard_6.png)

---
In the dashboard interface, by clicking on one or more values in the caption you can display only specific processes/containers monitored.
![](docs/figures/grafana/grafana_dashboard_7.png)
